use actix_web::{get, web, App, HttpServer};
use serde::{Deserialize, Serialize};
use std::sync::Mutex;

mod todolist;
use todolist::services;

struct AppState {
    todolist_entries: Mutex<Vec<TodoListEntry>>,
}
#[derive(Serialize, Deserialize, Clone)]
struct TodoListEntry {
    id: u32,
    title: String,
    date: i64,
    description: String,
    done: bool,
}

#[get("/")]
async fn index() -> &'static str {
    "Hello world!"
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_data = web::Data::new(AppState {
        todolist_entries: Mutex::new(vec![]),
    });

    HttpServer::new(move || {
        App::new()
            .app_data(app_data.clone())
            .service(index)
            .configure(services::init_routes)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
