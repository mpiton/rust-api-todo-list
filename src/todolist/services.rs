use super::models::{CreateEntryData, UpdateEntryData};
use crate::{AppState, TodoListEntry};
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};

#[get("/todolist/entries")]
async fn get_entries(data: web::Data<AppState>) -> impl Responder {
    let entries = data.todolist_entries.lock().unwrap();
    HttpResponse::Ok().json(entries.clone())
}

#[post("/todolist/entries")]
async fn create_entry(
    data: web::Data<AppState>,
    entry: web::Json<CreateEntryData>,
) -> impl Responder {
    let mut entries = data.todolist_entries.lock().unwrap();
    let id = entries.len() as u32 + 1;
    let new_entry = TodoListEntry {
        id,
        title: entry.title.clone(),
        date: entry.date,
        description: entry.description.clone(),
        done: false,
    };
    entries.push(new_entry.clone());
    HttpResponse::Ok().json(new_entry)
}

#[put("/todolist/entries/{id}")]
async fn update_entry(
    data: web::Data<AppState>,
    entry: web::Json<UpdateEntryData>,
    id: web::Path<u32>,
) -> impl Responder {
    let id = id.into_inner();
    let mut entries = data.todolist_entries.lock().unwrap();
    for i in 0..entries.len() {
        if entries[i].id == id {
            entries[i].title = entry.title.clone();
            entries[i].date = entry.date;
            entries[i].description = entry.description.clone();
            entries[i].done = entry.done;
            break;
        }
    }
    HttpResponse::Ok().finish()
}

#[delete("/todolist/entries/{id}")]
async fn delete_entry(data: web::Data<AppState>, id: web::Path<u32>) -> impl Responder {
    let mut entries = data.todolist_entries.lock().unwrap();
    let entry = entries.iter().position(|e| e.id == *id);
    match entry {
        Some(e) => {
            entries.remove(e);
            HttpResponse::Ok().finish()
        }
        None => HttpResponse::NotFound().finish(),
    }
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(get_entries);
    cfg.service(create_entry);
    cfg.service(update_entry);
    cfg.service(delete_entry);
}
